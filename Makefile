BINARY=rall

.PHONY: build linux run clean

build:
	go build -o $(BINARY) main.go

linux:
	GOOS=linux GOARCH=amd64 go build -o $(BINARY) main.go

run:
	go run main.go

clean:
	rm -f $(BINARY)
