#!/bin/bash

if [ $# -lt 2 ]; then
  echo "usage: $(basename "$0") <log-path> <count>"
  exit 1
fi

pushd "$(dirname "$0")" > /dev/null 2>&1

git clean -fd --quiet
git reset --hard --quiet
git pull --quiet
rall --log-path=$1 --count=$2
git add log.csv
git commit -m "Logged top r/all post $(date)"
git push origin

popd > /dev/null 2>&1
