# rall

*NOTE: This program is not currently running due to rate limit errors*

Retrieves and logs the current top post at https://www.reddit.com/r/all/top. This program is intended to be used in conjunction with a job scheduler (such as `cron`) to log data at periodic intervals.

## Usage
```
NAME:
   rall - Retrieves and logs top Reddit posts from r/all

USAGE:
   rall [global options] command [command options] [arguments...]

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --log-path value, -l value  absolute or relative path to the CSV log file (default: "./log.csv")
   --count value, -c value     number of posts to retrieve (default: 1)
   --help, -h                  show help (default: false)
```

## Installation

> All steps should be run on a server with `git` and `cron` installed. The automated authentication is inspired by [this article](https://alanedwardes.com/blog/posts/git-username-password-environment-variables/).

1. Create a custom Git credential helper shell script (`credential-helper.sh`) following the format:
    ```bash
    #!/bin/bash
    echo username=$GIT_USERNAME
    echo password=$GIT_PASSWORD
    ```
1. Clone this repository
1. Configure the repository to use the custom credential helper shell script:
    ```bash
    git config credential.helper "/bin/bash /full/path/to/credential-helper.sh"
    ```
1. Configure the name and email for the repository:
    ```bash
    git config user.name "Full Name"
    git config user.email "test@test.com"
    ```
1. Install the `rall` binary somewhere in the `PATH` (such as `/usr/local/bin`)
1. Add a cron job that executes `run.sh`:
    ```bash
    # Run the job every day at noon
    0 12 * * * export PATH="/usr/local/bin:$PATH"; /full/path/to/run.sh /full/path/to/log.csv 1 >> /full/path/to/rall_cron.log 2>&1
    ```
