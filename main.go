package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/urfave/cli/v2"
)

type Post struct {
	Data struct {
		Children []struct {
			Fields struct {
				Subreddit string  `json:"subreddit"`
				Title     string  `json:"title"`
				Score     int     `json:"score"`
				Created   float64 `json:"created"`
				ID        string  `json:"id"`
				Author    string  `json:"author"`
				Permalink string  `json:"permalink"`
				URL       string  `json:"url"`
			} `json:"data"`
		} `json:"children"`
	} `json:"data"`
}

func getTopPosts(client *http.Client, count int) (*Post, error) {
	endpoint := url.URL{
		Scheme: "https",
		Host:   "www.reddit.com",
		Path:   "r/all/top.json",
		RawQuery: url.Values{
			"t":     []string{"day"},
			"limit": []string{strconv.Itoa(count)},
		}.Encode(),
	}

	req, err := http.NewRequest(http.MethodGet, endpoint.String(), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Set("user-agent", "cli:rall")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unable to retrieve data with status code %s", resp.Status)
	}

	var data Post
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return nil, err
	}

	return &data, nil
}

func writeToCSV(post *Post, filepath string) error {
	file, err := os.OpenFile(filepath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}

	w := csv.NewWriter(file)

	for _, record := range post.Data.Children {
		row := []string{
			strconv.Itoa(int(time.Now().Unix())),
			record.Fields.Subreddit,
			record.Fields.Title,
			strconv.Itoa(record.Fields.Score),
			strconv.Itoa(int(record.Fields.Created)),
			record.Fields.ID,
			record.Fields.Author,
			record.Fields.Permalink,
			record.Fields.URL,
		}
		if err := w.Write(row); err != nil {
			return err
		}
	}

	w.Flush()

	if err := w.Error(); err != nil {
		return err
	}

	return nil
}

func main() {
	app := &cli.App{
		Name:  "rall",
		Usage: "Retrieves and logs top Reddit posts from r/all",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "log-path",
				Aliases: []string{"l"},
				Value:   "./log.csv",
				Usage:   "absolute or relative path to the CSV log file",
			},
			&cli.IntFlag{
				Name:    "count",
				Aliases: []string{"c"},
				Value:   1,
				Usage:   "number of posts to retrieve",
			},
		},
		Action: func(c *cli.Context) error {
			client := &http.Client{Timeout: 10 * time.Second}

			post, err := getTopPosts(client, c.Int("count"))
			if err != nil {
				return err
			}

			writeToCSV(post, c.String("log-path"))
			if err != nil {
				return err
			}

			for _, record := range post.Data.Children {
				log.Println("Successfully logged post", record.Fields.ID)
			}

			return nil
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
